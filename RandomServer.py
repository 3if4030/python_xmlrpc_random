
import random

from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler

# Restrict to a particular path.
class RequestHandler( SimpleXMLRPCRequestHandler ):
    rpc_paths = ( '/RPC2', )

# Create server
with SimpleXMLRPCServer(( 'localhost', 8000 ),
                        requestHandler = RequestHandler,
                        allow_none = True ) as server:
  class RandomServer:
    min = 1
    max = 100

    def setBounds( self, min, max ):
      if min <= max:
        self.min = min
        self.max = max
    
    def nextRandom( self ):
      return random.randint( self.min, self.max )

  server.register_instance( RandomServer() )

  server.serve_forever()
